package com.Assignment.ImplementedClass;

import com.Assignment.interfaces.ItemTypeTaxCalculator;
import com.Assignment.pojo.Item;

public class Raw implements ItemTypeTaxCalculator {
 final double TAXRATE=0.125;
	@Override
	public double taxCalculation(Item item) { //tax calculation of raw type
		
		double tax=TAXRATE*(item.getItem_price());
		
		return tax;
	}

}
