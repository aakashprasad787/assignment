package com.Assignment.ImplementedClass;

import com.Assignment.interfaces.ItemTypeTaxCalculator;
import com.Assignment.pojo.Item;

public class Manufactured implements ItemTypeTaxCalculator {//tax calculation of manufactured type
    final double TAXRATE=0.125;
    final double ADDITIONALRATE=0.02;
	@Override
	public double taxCalculation(Item item) {
		double tax;
		double temporaryTax=TAXRATE*(item.getItem_price());
		tax=temporaryTax+ADDITIONALRATE*(item.getItem_price()+temporaryTax);
		return tax;
	}

}
