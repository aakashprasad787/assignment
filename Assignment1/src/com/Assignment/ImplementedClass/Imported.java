package com.Assignment.ImplementedClass;

import com.Assignment.interfaces.ItemTypeTaxCalculator;
import com.Assignment.pojo.Item;

public class Imported implements ItemTypeTaxCalculator {

	final double IMPORTDUTY=0.1; 
	@Override
	public double taxCalculation(Item item) {
		double surcharge;
		double importDutyPrice=IMPORTDUTY*item.getItem_price();
		double price=importDutyPrice+item.getItem_price();
		if(price>0.0&&price<100.0)
			surcharge=5.0;
		else if(price>100.0&&price<200.0)
			surcharge =10.0;
		else
			surcharge=0.05*price;
		
		
		return importDutyPrice+surcharge;
	}

}
