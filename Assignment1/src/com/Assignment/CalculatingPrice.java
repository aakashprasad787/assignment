package com.Assignment;

import com.Assignment.ImplementedClass.*;
import com.Assignment.interfaces.ItemTypeTaxCalculator;
import com.Assignment.pojo.Item;

public class CalculatingPrice {
	/**calculating the total price of the item including the number of item of that particular item
	     and setting the final price and setting the tax on the item 
	     */
	public void totalPrice(Item item){ 
		ItemTypeTaxCalculator calculateTax=null;
		if(item.getItem_type().equalsIgnoreCase("raw"))
			calculateTax=new Raw();
		else if(item.getItem_type().equalsIgnoreCase("manufactured"))
			calculateTax=new Manufactured();
		else
			calculateTax=new Imported();
	
		item.setTax(calculateTax.taxCalculation(item));
		double finalPrice=(calculateTax.taxCalculation(item)+item.getItem_price())*item.getItem_quantity();		
	    item.setItem_price(finalPrice);
	}

}
