package com.Assignment.pojo;

public class Item {
private String item_name,item_type;
private int item_quantity;
private double item_price;
private double salesTax;
public Item(String item_name, String item_type, int item_quantity, double item_price) { //parameterized constructor
	super();
	this.item_name = item_name;
	this.item_type = item_type;
	this.item_quantity = item_quantity;
	this.item_price = item_price;
}
public String getItem_name() {
	return item_name;
}
public void setItem_name(String item_name) {
	this.item_name = item_name;
}
public String getItem_type() {
	return item_type;
}
public void setItem_type(String item_type) {
	this.item_type = item_type;
}
public int getItem_quantity() {
	return item_quantity;
}
public void setItem_quantity(int item_quantity) {
	this.item_quantity = item_quantity;
}
public double getItem_price() {
	return item_price;
}
public void setItem_price(double item_price) {
	this.item_price = item_price;
}
public void setTax(double salesTax)
{
	this.salesTax=salesTax;
}
public double getTax()
{
	return salesTax;
}
@Override
public String toString() {
	return "Item [item_name=" + item_name + ", item_type=" + item_type + ", item_quantity=" + item_quantity
			+ ",final item_price=" + this.getItem_price()+",salestax= "+this.getTax()+"]";
}

}
