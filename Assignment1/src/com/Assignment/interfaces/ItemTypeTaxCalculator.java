package com.Assignment.interfaces;

import com.Assignment.pojo.Item;

// interface for calculating the tax .
public interface ItemTypeTaxCalculator {
	double taxCalculation(Item item);

}
